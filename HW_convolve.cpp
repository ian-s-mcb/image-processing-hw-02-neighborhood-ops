#include <cstdio>
extern void memoryFailure();
extern int insertRowBuffer(int, int, uchar *, int, int, int, ChannelPtr<uchar>);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_convolve:
//
// Apply image convolution on I1. Output is in I2.
//
void
HW_convolve(ImagePtr I1, ImagePtr Ikernel, ImagePtr I2)
{
  // Get input dimensions
  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();

  int ch, type;
  ChannelPtr<uchar> src, dst;

  // Kernel variables
  int kernelDim;
  ChannelPtr<float> kernel, kernelPtrTmp;
  IP_getChannel(Ikernel, ch, kernel, type);
  kernelDim = Ikernel->width();
  int ky, kx;

  // Buffer variables
  void *ptr;
  int numOfRows = kernelDim;
  int rowPadding = (numOfRows - 1) / 2;
  int rowWidthPlusPadding = w + (2 * rowPadding);
  int rowBufferIndex = 0;
  int rowCutOff = h - rowPadding - 2;
  uchar *rowBuffer, *rowBufferPtrTmp;

  float sum;
  uchar clippedSum;
  int i, x, y;

  // Allocate buffer
  ptr = std::malloc(sizeof(uchar) * rowWidthPlusPadding * numOfRows);
  if (ptr == NULL) memoryFailure();
  rowBuffer = (uchar *)ptr;

  // For each channel
  for(ch = 0; IP_getChannel(I1, ch, src, type); ch++) {
    IP_getChannel(I2, ch, dst, type);

    /* Fill row buffer;
    Rows before kernel center are replicated, so the data being inserted
    is unchanged (ie. no src ptr incrementing). */
    for(i = 0; i < kernelDim; i++) {
      rowBufferIndex = insertRowBuffer(rowPadding, w, rowBuffer, rowBufferIndex, numOfRows, rowWidthPlusPadding, src);
      if (i >= rowPadding) src += w;
    }

    // Visit each image row
    for(y = 0; y < h; y++) {
      // Visit each image column
      for(x = 0; x < w; x++) {
        rowBufferPtrTmp = rowBuffer + x;
        kernelPtrTmp = kernel;
        sum = 0;

        // Compute product of kernel and row buffer and keep a running sum
        for(ky = 0; ky < kernelDim; ky++) {
          for(kx = 0; kx < kernelDim; kx++) {
            sum += *kernelPtrTmp * *rowBufferPtrTmp;
            rowBufferPtrTmp++;
            kernelPtrTmp++;
          }
          rowBufferPtrTmp += rowWidthPlusPadding - kernelDim;
        }

        // Round, clip, store the sum
        sum = ROUND(sum);
        clippedSum = CLIP(sum, 0, MaxGray);
        *dst++ = clippedSum;
      } // end x loop

      /* Insert into row buffer;
      Rows after after rowCutOff are replicated, so the data being
      inserted is unchanged (ie. no src ptr incrementing). The value of
      rowCutOff was determined experimentally. */
      rowBufferIndex = insertRowBuffer(rowPadding, w, rowBuffer, rowBufferIndex, numOfRows, rowWidthPlusPadding, src);
      if (y < rowCutOff) src += w;
    } // end y loop
  } // end channel loop

  // Deallocate buffer
  free(rowBuffer);
}
