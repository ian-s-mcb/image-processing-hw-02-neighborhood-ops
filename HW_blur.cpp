void blur1D(ChannelPtr<uchar>, int, int, int, ChannelPtr<uchar>);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_blur:
//
// Blur image I1 with a box filter (unweighted averaging).
// The filter has width filterW and height filterH.
// We force the kernel dimensions to be odd.
// Output is in I2.
//
void
HW_blur(ImagePtr I1, int filterW, int filterH, ImagePtr I2)
{
  // Force kernel dimensions to be odd
  if (filterW % 2 == 0) filterW++;
  if (filterH % 2 == 0) filterH++;

  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();

  int type, i, j;
  ChannelPtr<uchar> src, dst;

  // For each channel
  for(int ch = 0; IP_getChannel(I1, ch, src, type); ch++) {
    IP_getChannel(I2, ch, dst, type);

    // Apply horizontal blurring
    if (filterW > 2) {
      for(i = 0, j = 0; i < h; i++, j += w)
        blur1D(src + j, w, 1, filterW, dst + j);
    }
    else IP_copyChannel(I1, ch, I2, ch);

    // Apply vertical blurring
    if (filterH > 2) {
      for(i = 0; i < w; i++) {
        blur1D(dst + i, h, w, filterH, dst + i);
      }
    }
  }
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// blur1D:
//
// Blur src with 1D box filter of width ww.
//
void
blur1D(ChannelPtr<uchar> src, int len, int stride, int ww, ChannelPtr<uchar> dst)
{
  int i, j;
  int bufferWidth = len + ww - 1;
  uchar buffer[bufferWidth];

  // Fill bufffer with padding (using pixel replication)
  int padding = (ww - 1) / 2;
  int firstPixel = src[0];
  int lastPixel = src[(len - 1) * stride];
  for(i = 0              ; i < padding    ; i++) buffer[i] = firstPixel;
  for(i = (padding + len); i < bufferWidth; i++) buffer[i] = lastPixel;

  // Fill buffer with src pixels
  for(i=padding, j=0; i < padding + len; i++, j += stride) buffer[i] = src[j];

  // Compute unweighted averages (using running sum)
  int sum = 0;
  for(i = 0; i < ww ; i++) sum += buffer[i];
  dst[0] = sum / ww;
  for(i = 1, j = stride; i < len; i++, j += stride) {
    sum += buffer[i + ww - 1] - buffer[i - 1];
    dst[j] = sum / ww;
  }
}
