#include <algorithm>


int insertRowBuffer(int, int, uchar *, int, int, int, ChannelPtr<uchar>);
extern void memoryFailure();


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_median:
//
// Apply median filter of size sz x sz to I1.
// Clamp sz to 9.
// Output is in I2.
//
void
HW_median(ImagePtr I1, int sz, ImagePtr I2)
{
  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();

  int ch, type;
  ChannelPtr<uchar> src, dst;

  // Validate sz
  sz = CLIP(sz, 3, 9);

  // General variables
  int i, x, y;

  // Row buffer variables
  void *ptr;
  int numOfRows = sz;
  int rowPadding = (numOfRows - 1) / 2;
  int rowWidthPlusPadding = w + (2 * rowPadding);
  int rowBufferIndex = 0;
  int rowCutOff = h - rowPadding - 2;
  uchar *rowBuffer, *rowBufferPtrTmp;

  // Kernel buffer variables
  int kernelDim = numOfRows;
  int kernelCenterDim = rowPadding;
  uchar *kernel, *kernelPtrTmp, *kernelPtrEnd, *kernelPtrCenter;
  int ky, kx;

  // Allocate row buffer
  ptr = std::malloc(sizeof(uchar) * numOfRows * rowWidthPlusPadding);
  if (ptr == NULL) memoryFailure();
  rowBuffer = (uchar *)ptr;

  // Allocate kernel
  ptr = std::malloc(sizeof(uchar) * kernelDim * kernelDim);
  if (ptr == NULL) memoryFailure();
  kernel = (uchar *)ptr;
  kernelPtrEnd = kernel + (kernelDim * kernelDim);
  kernelPtrCenter = kernel + (kernelDim * kernelCenterDim) + kernelCenterDim;

  // For each channel
  for(ch = 0; IP_getChannel(I1, ch, src, type); ch++) {
    IP_getChannel(I2, ch, dst, type);

    /* Fill row buffer;
    Rows before kernel center are replicated, so the data being inserted
    is unchanged (ie. no src ptr incrementing). */
    for(i = 0; i < kernelDim; i++) {
      rowBufferIndex = insertRowBuffer(rowPadding, w, rowBuffer, rowBufferIndex, numOfRows, rowWidthPlusPadding, src);
      if (i >= kernelCenterDim) src += w;
    }

    // Visit each image row
    for(y = 0; y < h; y++) {
      // Visit each image column
      for(x = 0; x < w; x++) {
        rowBufferPtrTmp = rowBuffer + x;
        kernelPtrTmp = kernel;

        // Fill kernel
        for(ky = 0; ky < kernelDim; ky++) {
          for(kx = 0; kx < kernelDim; kx++) {
            *kernelPtrTmp++ = *rowBufferPtrTmp++;
          }
          rowBufferPtrTmp += rowWidthPlusPadding - kernelDim;
        }

        // Sort kernel, store center pixel
        std::sort(kernel, kernelPtrEnd);
        *dst++ = *kernelPtrCenter;
      } // end x loop

      /* Insert into row buffer;
      Rows after after rowCutOff are replicated, so the data being
      inserted is unchanged (ie. no src ptr incrementing). The value of
      rowCutOff was determined experimentally. */
      rowBufferIndex = insertRowBuffer(rowPadding, w, rowBuffer, rowBufferIndex, numOfRows, rowWidthPlusPadding, src);
      if (y < rowCutOff) src += w;
    } // end y loop
  } // end channel loop

  // Deallocate buffer, kernel
  free(rowBuffer);
  free(kernel);
} // end HW_median function


// -----------------------
// --- Buffer function ---
// -----------------------
int
insertRowBuffer(int rowPadding, int rowWidth, uchar *buffer, int bufferIndex, int numOfRows, int rowWidthPlusPadding, ChannelPtr<uchar> data) {
  int i;
  uchar *ptr1, *ptr2;
  ChannelPtr<uchar> ptrData, ptrDataEnd;

  ptr1 = buffer + (bufferIndex % numOfRows) * rowWidthPlusPadding;

  // Replicate initial, final data values
  ptr2 = ptr1;
  for(i = 0; i < rowPadding; i++, ptr2++) *ptr2 =  data[0];
  ptr2 = ptr1 + rowWidth + rowPadding;
  for(i = 0; i < rowPadding; i++, ptr2++) *ptr2 = data[rowWidth - 1];

  // Fill data
  ptr2 = ptr1 + rowPadding;
  ptrData = data;
  ptrDataEnd = data + rowWidth;
  for(; ptrData < ptrDataEnd; ptr2++, ptrData++) *ptr2 = *ptrData;

  return bufferIndex + 1;
}
