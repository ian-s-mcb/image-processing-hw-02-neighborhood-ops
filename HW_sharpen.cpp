extern void HW_blur  (ImagePtr, int, int, ImagePtr);
extern void HW_blur1D(ChannelPtr<uchar>, int, int, int, ChannelPtr<uchar>);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// HW_sharpen:
//
// Sharpen image I1. Output is in I2.
//
void
HW_sharpen(ImagePtr I1, int size, double factor, ImagePtr I2)
{
  IP_copyImageHeader(I1, I2);
  int w = I1->width ();
  int h = I1->height();
  int total = w * h;

  // Obtain blurred version of the original image
  HW_blur(I1, size, size, I2);

  int type, val;
  ChannelPtr<uchar> src, dst, endd;

  // For each channel
  for(int ch = 0; IP_getChannel(I1, ch, src, type); ch++) {
    IP_getChannel(I2, ch, dst, type);
    for(endd = src + total; src<endd; dst++, src++) {
      /* Apply sharpen formula;
      Subtract the blurred version from the original version, mulitply the
      difference by factor, then add the result to the original version.

      To avoid uchar overflow, the formula is computed using int, instead
      of uchar. Clipping is performed to keep the new intesity value
      within the range [0, 255]. */
      val = factor * ((int)*src - *dst) + *src;
      val = CLIP(val, 0, MaxGray);
      *dst = val;
    }
  }
}
